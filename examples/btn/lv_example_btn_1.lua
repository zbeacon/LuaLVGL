local lvgl = require( "lvgl" )

lvapi = lvgl.api

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

local btn1 = lvapi.lv_btn_create( lvapi.lv_scr_act() )

lvapi.lv_obj_align( btn1, lvapi.LV_ALIGN_CENTER, 0, -40 )

lvapi.lv_obj_add_event_cb( btn1, lvapi.LV_EVENT_CLICKED, function( e )

	print( "Clicked" )
end )

local label = lvapi.lv_label_create( btn1 )

lvapi.lv_label_set_text( label, "Button" )

lvapi.lv_obj_center( label )

local btn2 = lvapi.lv_btn_create( lvapi.lv_scr_act() )

lvapi.lv_obj_align( btn2, lvapi.LV_ALIGN_CENTER, 0, 40 )

lvapi.lv_obj_add_flag( btn2, lvapi.LV_OBJ_FLAG_CHECKABLE )

lvapi.lv_obj_set_height( btn2, lvapi.LV_SIZE_CONTENT )

label = lvapi.lv_label_create( btn2 )

lvapi.lv_label_set_text( label, "Toggle" )

lvapi.lv_obj_center( label )

lvapi.lv_obj_add_event_cb( btn2, lvapi.LV_EVENT_VALUE_CHANGED, function( e )

	print( "Toggled" )
end )

lvgl.loop()
