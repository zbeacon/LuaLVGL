local lvgl = require( "lvgl" )

setmetatable( _G, { __index = function( self, index )

	return rawget( self, index ) or rawget( lvgl.api, index )
end } )

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

local scr = lv_scr_act()

local style = lv_style_init()

lv_style_set_radius( style, 3 )

lv_style_set_bg_opa( style, LV_OPA_100 )

lv_style_set_bg_color( style, lv_palette_main( LV_PALETTE_BLUE ) )

lv_style_set_bg_grad_color( style, lv_palette_darken( LV_PALETTE_BLUE, 2 ) )

lv_style_set_bg_grad_dir( style, LV_GRAD_DIR_VER )

lv_style_set_border_opa( style, LV_OPA_40 )

lv_style_set_border_width( style, 2 )

lv_style_set_border_color( style, lv_palette_main( LV_PALETTE_GREY ) )

lv_style_set_shadow_width( style, 8 )

lv_style_set_shadow_color( style, lv_palette_main( LV_PALETTE_GREY ) )

lv_style_set_shadow_ofs_y( style, 8 )

lv_style_set_outline_opa( style, LV_OPA_COVER )

lv_style_set_outline_color( style, lv_palette_main( LV_PALETTE_BLUE ) )

lv_style_set_text_color( style, lv_color_white() )

lv_style_set_pad_all( style, 10 )

local style_pr = lv_style_init()

lv_style_set_outline_width( style_pr, 30 )

lv_style_set_outline_opa( style_pr, LV_OPA_TRANSP )

lv_style_set_translate_y( style_pr, 5 )

lv_style_set_shadow_ofs_y( style_pr, 3 )

lv_style_set_bg_color( style_pr, lv_palette_darken( LV_PALETTE_BLUE, 2 ) )

lv_style_set_bg_grad_color( style_pr, lv_palette_darken( LV_PALETTE_BLUE, 4 ) )

local trans = lv_style_transition_dsc_init( { LV_STYLE_OUTLINE_WIDTH, LV_STYLE_OUTLINE_OPA }, "lv_anim_path_linear", 300, 0, nil )

lv_style_set_transition( style_pr, trans )

local btn1 = lv_btn_create( scr )

lv_obj_remove_style_all( btn1 )

lv_obj_add_style( btn1, style, 0 )

lv_obj_add_style( btn1, style_pr, LV_STATE_PRESSED )

lv_obj_set_size( btn1, LV_SIZE_CONTENT, LV_SIZE_CONTENT )

lv_obj_center( btn1 )

local label = lv_label_create( btn1 )

lv_label_set_text( label, "Button" )

lv_obj_center( label )

lv_obj_add_event_cb( btn1, LV_EVENT_CLICKED, function( e )

	print( "Clicked" )
end )

lvgl.loop()
