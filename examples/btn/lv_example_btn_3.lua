local lvgl = require( "lvgl" )

setmetatable( _G, { __index = function( self, index )

	return rawget( self, index ) or rawget( lvgl.api, index )
end } )

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

local scr = lv_scr_act()

local props = { LV_STYLE_TRANSFORM_WIDTH, LV_STYLE_TRANSFORM_HEIGHT, LV_STYLE_TEXT_LETTER_SPACE }

transition_dsc_def = lv_style_transition_dsc_init( props, "lv_anim_path_overshoot", 250, 100 )

transition_dsc_pr = lv_style_transition_dsc_init( props, "lv_anim_path_ease_in_out", 250, 0 )

style_def = lv_style_init()

lv_style_set_transition( style_def, transition_dsc_def )

style_pr = lv_style_init()

lv_style_set_transform_width( style_pr, 10 )

lv_style_set_transform_height( style_pr, -10 )

lv_style_set_text_letter_space( style_pr, 10 )

lv_style_set_transition( style_pr, transition_dsc_pr )

local btn1 = lv_btn_create( scr )

lv_obj_align( btn1, LV_ALIGN_CENTER, 0, -80 )

lv_obj_add_style( btn1, style_pr, LV_STATE_PRESSED )

lv_obj_add_style( btn1, style_def, 0 )

local label = lv_label_create( btn1 )

lv_label_set_text( label, "Gum" )

lv_obj_add_event_cb( btn1, LV_EVENT_CLICKED, function( e )

	print( "Clicked" )
end )

lvgl.loop()
