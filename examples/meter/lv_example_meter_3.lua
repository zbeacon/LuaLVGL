local lvgl = require( "lvgl" )

setmetatable( _G, { __index = function( self, index )

	return rawget( self, index ) or rawget( lvgl.api, index )
end } )

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

local scr = lv_scr_act()

local meter = lv_meter_create( scr )

lv_obj_set_size( meter, 220, 220 )

lv_obj_center( meter )

-- Create a scale for the minutes
-- 61 ticks in a 360 degrees range (the last and the first line overlaps)

local scale_min = lv_meter_add_scale( meter )

lv_meter_set_scale_ticks( meter, scale_min, 61, 1, 10, lv_palette_main( LV_PALETTE_GREY ) )

lv_meter_set_scale_range( meter, scale_min, 0, 60, 360, 270 )

-- Create another scale for the hours. It's only visual and contains only major ticks

local scale_hour = lv_meter_add_scale( meter )

lv_meter_set_scale_ticks( meter, scale_hour, 12, 0, 0, lv_palette_main( LV_PALETTE_GREY ) )	-- 12 ticks

lv_meter_set_scale_major_ticks( meter, scale_hour, 1, 2, 20, lv_color_black(), 10 )		-- Every tick is major

lv_meter_set_scale_range( meter, scale_hour, 1, 12, 330, 300 )					-- [1..12] values in an almost full circle

-- Add a the hands from images

-- 这里要解释一下，在 Windows 下表示路径，需要加前缀 W

local indic_min = lv_meter_add_needle_img( meter, scale_min, "W../assets/img_hand_min.png", 5, 5 )

local indic_hour = lv_meter_add_needle_img( meter, scale_min, "W../assets/img_hand_hour.png", 5, 5 )

-- Create an animation to set the value

local a = lv_anim_init()

lv_anim_set_exec_cb( a, function( indic, value )

	lv_meter_set_indicator_end_value( meter, indic, value )
end )

lv_anim_set_values( a, 0, 60 )

lv_anim_set_repeat_count( a, LV_ANIM_REPEAT_INFINITE )

lv_anim_set_time( a, 2000 )	-- 2 sec for 1 turn of the minute hand (1 hour)

lv_anim_set_var( a, indic_min )

lv_anim_start( a )

lv_anim_set_var( a, indic_hour )

lv_anim_set_time( a, 24000 )	-- 24 sec for 1 turn of the hour hand

lv_anim_set_values( a, 0, 60 )

lv_anim_start( a )

lvgl.loop()