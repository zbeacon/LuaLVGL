local lvgl = require( "lvgl" )

setmetatable( _G, { __index = function( self, index )

	return rawget( self, index ) or rawget( lvgl.api, index )
end } )

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

local scr = lv_scr_act()

local meter = lv_meter_create( scr )

lv_obj_center( meter )

lv_obj_set_size( meter, 200, 200 )

-- Add a scale first

local scale = lv_meter_add_scale( meter )

lv_meter_set_scale_ticks( meter, scale, 41, 2, 10, lv_palette_main( LV_PALETTE_GREY ) )

lv_meter_set_scale_major_ticks( meter, scale, 8, 4, 15, lv_color_black(), 10 )

-- Add a blue arc to the start

local indic = lv_meter_add_arc( meter, scale, 3, lv_palette_main( LV_PALETTE_BLUE ), 0 )

lv_meter_set_indicator_start_value( meter, indic, 0 )

lv_meter_set_indicator_end_value( meter, indic, 20 )

-- Make the tick lines blue at the start of the scale

indic = lv_meter_add_scale_lines( meter, scale, lv_palette_main( LV_PALETTE_BLUE ), lv_palette_main( LV_PALETTE_BLUE ), false, 0 )

lv_meter_set_indicator_start_value( meter, indic, 0 )

lv_meter_set_indicator_end_value( meter, indic, 20 )

-- Add a red arc to the end

indic = lv_meter_add_arc( meter, scale, 3, lv_palette_main( LV_PALETTE_RED ), 0 )

lv_meter_set_indicator_start_value( meter, indic, 80 )

lv_meter_set_indicator_end_value( meter, indic, 100 )

-- Make the tick lines red at the end of the scale

indic = lv_meter_add_scale_lines( meter, scale, lv_palette_main( LV_PALETTE_RED ), lv_palette_main( LV_PALETTE_RED ), false, 0 )

lv_meter_set_indicator_start_value( meter, indic, 80 )

lv_meter_set_indicator_end_value( meter, indic, 100 )

-- Add a needle line indicator

indic = lv_meter_add_needle_line( meter, scale, 4, lv_palette_main( LV_PALETTE_GREY ), -10 )

-- Create an animation to set the value

local a = lv_anim_init()

lv_anim_set_exec_cb( a, function( indic, value )

	lv_meter_set_indicator_value( meter, indic, value )
end )

lv_anim_set_var( a, indic )

lv_anim_set_values( a, 0, 100 )

lv_anim_set_time( a, 2000 )

lv_anim_set_repeat_delay( a, 100 )

lv_anim_set_playback_time( a, 500 )

lv_anim_set_playback_delay( a, 100 )

lv_anim_set_repeat_count( a, LV_ANIM_REPEAT_INFINITE )

lv_anim_start( a )

lvgl.loop()