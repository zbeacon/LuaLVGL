local lvgl = require( "lvgl" )

setmetatable( _G, { __index = function( self, index )

	return rawget( self, index ) or rawget( lvgl.api, index )
end } )

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

local scr = lv_scr_act()

local meter = lv_meter_create( scr )

-- Remove the background and the circle from the middle

lv_obj_remove_style( meter, nil, LV_PART_MAIN      )
lv_obj_remove_style( meter, nil, LV_PART_INDICATOR )

lv_obj_set_size( meter, 200, 200 )

lv_obj_center( meter )

-- Add a scale first with no ticks.

local scale = lv_meter_add_scale( meter )

lv_meter_set_scale_ticks( meter, scale, 0, 0, 0, lv_color_black() )

lv_meter_set_scale_range( meter, scale, 0, 100, 360, 0 )

-- Add a three arc indicator

local indic_w = 100

local indic1 = lv_meter_add_arc( meter, scale, indic_w, lv_palette_main( LV_PALETTE_ORANGE ), 0 )

lv_meter_set_indicator_start_value( meter, indic1, 0 )

lv_meter_set_indicator_end_value( meter, indic1, 40 )

local indic2 = lv_meter_add_arc( meter, scale, indic_w, lv_palette_main( LV_PALETTE_YELLOW ), 0 )

lv_meter_set_indicator_start_value( meter, indic2, 40 )

lv_meter_set_indicator_end_value( meter, indic2, 80 )

local indic3 = lv_meter_add_arc( meter, scale, indic_w, lv_palette_main( LV_PALETTE_DEEP_ORANGE ), 0 )

lv_meter_set_indicator_start_value( meter, indic3, 80 )

lv_meter_set_indicator_end_value( meter, indic3, 100 )

lvgl.loop()