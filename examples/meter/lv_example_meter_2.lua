local lvgl = require( "lvgl" )

setmetatable( _G, { __index = function( self, index )

	return rawget( self, index ) or rawget( lvgl.api, index )
end } )

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

local scr = lv_scr_act()

local meter = lv_meter_create( scr )

lv_obj_center( meter )

lv_obj_set_size( meter, 200, 200 )

-- Remove the circle from the middle

lv_obj_remove_style( meter, nil, LV_PART_INDICATOR )

-- Add a scale first

local scale = lv_meter_add_scale( meter )

lv_meter_set_scale_ticks( meter, scale, 11, 2, 10, lv_palette_main( LV_PALETTE_GREY ) )

lv_meter_set_scale_major_ticks( meter, scale, 1, 2, 30, lv_color_hex3( 0xeee ), 15 )

lv_meter_set_scale_range( meter, scale, 0, 100, 270, 90 )

-- Add a three arc indicator

local indic1 = lv_meter_add_arc( meter, scale, 10, lv_palette_main( LV_PALETTE_RED   ),   0 )
local indic2 = lv_meter_add_arc( meter, scale, 10, lv_palette_main( LV_PALETTE_GREEN ), -10 )
local indic3 = lv_meter_add_arc( meter, scale, 10, lv_palette_main( LV_PALETTE_BLUE  ), -20 )

-- Create an animation to set the value

local a = lv_anim_init()

lv_anim_set_exec_cb( a, function( indic, value )

	lv_meter_set_indicator_end_value( meter, indic, value )
end )

lv_anim_set_values( a, 0, 100 )

lv_anim_set_repeat_delay( a, 100 )

lv_anim_set_playback_delay( a, 100 )

lv_anim_set_repeat_count( a, LV_ANIM_REPEAT_INFINITE )

lv_anim_set_time( a, 2000 )

lv_anim_set_playback_time( a, 500 )

lv_anim_set_var( a, indic1 )

lv_anim_start( a )

lv_anim_set_time( a, 1000 )

lv_anim_set_playback_time( a, 1000 )

lv_anim_set_var( a, indic2 )

lv_anim_start( a )

lv_anim_set_time( a, 1000 )

lv_anim_set_playback_time( a, 2000 )

lv_anim_set_var( a, indic3 )

lv_anim_start( a )

lvgl.loop()