local lvgl = require( "lvgl" )

setmetatable( _G, { __index = function( self, index )

	return rawget( self, index ) or rawget( lvgl.api, index )
end } )

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

local scr = lv_scr_act()

local btn_cnt = 1

local menu = lv_menu_create( scr )

lv_obj_set_size( menu, lv_disp_get_hor_res(), lv_disp_get_ver_res() )

lv_obj_center( menu )

local sub_page = lv_menu_page_create( menu )

local cont = lv_menu_cont_create( sub_page )

local label = lv_label_create( cont )

lv_label_set_text( label, "Hello, I am hiding inside the first item" )

local main_page = lv_menu_page_create( menu )

cont = lv_menu_cont_create( main_page )

label = lv_label_create( cont )

lv_label_set_text( label, "Item 1" )

lv_menu_set_load_page_event( menu, cont, sub_page )

lv_menu_set_page( menu, main_page )

local float_btn = lv_btn_create( scr )

lv_obj_set_size( float_btn, 50, 50 )

lv_obj_add_flag( float_btn, LV_OBJ_FLAG_FLOATING )

lv_obj_align( float_btn, LV_ALIGN_BOTTOM_RIGHT, -10, -10 )

lv_obj_add_event_cb( float_btn, LV_EVENT_CLICKED, function( e )

	btn_cnt = btn_cnt + 1

	local sub_page = lv_menu_page_create( menu )

	local cont = lv_menu_cont_create( sub_page )

	local label = lv_label_create( cont )

	lv_label_set_text( label, string.format( "Hello, I am hiding inside %s", btn_cnt ) )

	cont = lv_menu_cont_create( main_page )

	label = lv_label_create( cont )

	lv_label_set_text( label, string.format( "Item %s", btn_cnt ) )

	lv_menu_set_load_page_event( menu, cont, sub_page )

	lv_obj_scroll_to_view_recursive( cont, LV_ANIM_ON )
end )

lv_obj_set_style_radius( float_btn, LV_RADIUS_CIRCLE, 0 )

lv_obj_set_style_bg_img_src( float_btn, LV_SYMBOL_PLUS, 0 )

lv_obj_set_style_text_font( float_btn, lv_theme_get_font_large( float_btn ), 0 )

lvgl.loop()