local lvgl = require( "lvgl" )

setmetatable( _G, { __index = function( self, index )

	return rawget( self, index ) or rawget( lvgl.api, index )
end } )

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

local scr = lv_scr_act()

local LV_MENU_ITEM_BUILDER_VARIANT_1 <const> = 0
local LV_MENU_ITEM_BUILDER_VARIANT_2 <const> = 1

function create_text( parent, icon, txt, builder_variant )

	local obj = lv_menu_cont_create( parent )

	local img = nil

	local label = nil

	if icon then

		img = lv_img_create( obj )

		lv_img_set_src( img, icon )
	end

	if txt then

		label = lv_label_create( obj )

		lv_label_set_text( label, txt )

		lv_label_set_long_mode( label, LV_LABEL_LONG_SCROLL_CIRCULAR )

		lv_obj_set_flex_grow( label, 1 )
	end

	if builder_variant == LV_MENU_ITEM_BUILDER_VARIANT_2 and icon and txt then

		lv_obj_add_flag( img, LV_OBJ_FLAG_FLEX_IN_NEW_TRACK )

		lv_obj_swap( img, label )
	end

	return obj
end

function create_slider( parent, icon, txt, min, max, val )

	local obj = create_text( parent, icon, txt, LV_MENU_ITEM_BUILDER_VARIANT_2 )

	local slider = lv_slider_create( obj )

	lv_obj_set_flex_grow( slider, 1 )

	lv_slider_set_range( slider, min, max )

	lv_slider_set_value( slider, val, LV_ANIM_OFF )

	if icon == nil then

		lv_obj_add_flag( slider, LV_OBJ_FLAG_FLEX_IN_NEW_TRACK )
	end

	return obj
end

function create_switch( parent, icon, txt, chk )

	local obj = create_text( parent, icon, txt, LV_MENU_ITEM_BUILDER_VARIANT_1 )

	local sw = lv_switch_create( obj )

	lv_obj_add_state( sw, chk and LV_STATE_CHECKED or 0 )

	return obj
end

local menu = lv_menu_create( scr )

local bg_color = lv_obj_get_style_bg_color( menu, 0 )

if lv_color_brightness( bg_color ) > 127 then

	lv_obj_set_style_bg_color( menu, lv_color_darken( lv_obj_get_style_bg_color( menu, 0 ), 10 ), 0 )
else

	lv_obj_set_style_bg_color( menu, lv_color_darken( lv_obj_get_style_bg_color( menu, 0 ), 50 ), 0 )
end

lv_menu_set_mode_root_back_btn( menu, LV_MENU_ROOT_BACK_BTN_ENABLED )

lv_obj_add_event_cb( menu, LV_EVENT_CLICKED, function( e )

	local obj = lv_event_get_target( e )

	if lv_menu_back_btn_is_root( menu, obj ) then

		local mbox1 = lv_msgbox_create( nil, "Hello", "Root back btn click.", nil, true )

		lv_obj_center( mbox1 )
	end
end )

lv_obj_set_size( menu, lv_disp_get_hor_res(), lv_disp_get_ver_res() )

lv_obj_center( menu )

local sub_mechanics_page = lv_menu_page_create( menu )

lv_obj_set_style_pad_hor( sub_mechanics_page, lv_obj_get_style_pad_left( lv_menu_get_main_header( menu ), 0 ), 0 )

lv_menu_separator_create( sub_mechanics_page )

local section = lv_menu_section_create( sub_mechanics_page )

create_slider( section, LV_SYMBOL_SETTINGS, "Velocity",     0, 150, 120 )
create_slider( section, LV_SYMBOL_SETTINGS, "Acceleration", 0, 150, 50  )
create_slider( section, LV_SYMBOL_SETTINGS, "Weight limit", 0, 150, 80  )

local sub_sound_page = lv_menu_page_create( menu )

lv_obj_set_style_pad_hor( sub_sound_page, lv_obj_get_style_pad_left( lv_menu_get_main_header( menu ), 0 ), 0 )

lv_menu_separator_create( sub_sound_page )

section = lv_menu_section_create( sub_sound_page )

create_switch( section, LV_SYMBOL_AUDIO, "Sound", false )

local sub_display_page = lv_menu_page_create( menu )

lv_obj_set_style_pad_hor( sub_display_page, lv_obj_get_style_pad_left( lv_menu_get_main_header( menu ), 0 ), 0 )

lv_menu_separator_create( sub_display_page )

section = lv_menu_section_create( sub_display_page )

create_slider( section, LV_SYMBOL_SETTINGS, "Brightness", 0, 150, 100 )

local sub_software_info_page = lv_menu_page_create( menu )

lv_obj_set_style_pad_hor( sub_software_info_page, lv_obj_get_style_pad_left( lv_menu_get_main_header( menu ), 0 ), 0 )

section = lv_menu_section_create( sub_software_info_page )

create_text( section, nil, "Version 1.0", LV_MENU_ITEM_BUILDER_VARIANT_1 )

local sub_legal_info_page = lv_menu_page_create( menu )

lv_obj_set_style_pad_hor( sub_legal_info_page, lv_obj_get_style_pad_left( lv_menu_get_main_header( menu ), 0 ), 0 )

section = lv_menu_section_create( sub_legal_info_page )

for i = 0, 14 do

	create_text( section, nil, "This is a long long long long long long long long long text, if it is long enough it may scroll.", LV_MENU_ITEM_BUILDER_VARIANT_1 )
end

local sub_about_page = lv_menu_page_create( menu )

lv_obj_set_style_pad_hor( sub_about_page, lv_obj_get_style_pad_left( lv_menu_get_main_header( menu ), 0 ), 0 )

lv_menu_separator_create( sub_about_page )

section = lv_menu_section_create( sub_about_page )

cont = create_text( section, nil, "Software information", LV_MENU_ITEM_BUILDER_VARIANT_1 )

lv_menu_set_load_page_event( menu, cont, sub_software_info_page )

cont = create_text( section, NULL, "Legal information", LV_MENU_ITEM_BUILDER_VARIANT_1 )

lv_menu_set_load_page_event( menu, cont, sub_legal_info_page )

local sub_menu_mode_page = lv_menu_page_create( menu )

lv_obj_set_style_pad_hor( sub_menu_mode_page, lv_obj_get_style_pad_left( lv_menu_get_main_header( menu ), 0 ), 0 )

lv_menu_separator_create( sub_menu_mode_page )

section = lv_menu_section_create( sub_menu_mode_page )

cont = create_switch( section, LV_SYMBOL_AUDIO, "Sidebar enable", true )

lv_obj_add_event_cb( lv_obj_get_child( cont, 2 ), LV_EVENT_VALUE_CHANGED, function( e )

	local code = lv_event_get_code( e )

	local obj = lv_event_get_target( e )

	if code == LV_EVENT_VALUE_CHANGED then

		if lv_obj_has_state( obj, LV_STATE_CHECKED ) then

			lv_menu_set_page( menu, nil )

			lv_menu_set_sidebar_page( menu, root_page )

			lv_event_send( lv_obj_get_child( lv_obj_get_child( lv_menu_get_cur_sidebar_page( menu ), 0 ), 0 ), LV_EVENT_CLICKED, nil )
		else

			lv_menu_set_sidebar_page( menu, nil )

			lv_menu_clear_history( menu )

			lv_menu_set_page( menu, root_page )
		end
	end
end )

root_page = lv_menu_page_create( menu, "Settings" )

lv_obj_set_style_pad_hor( root_page, lv_obj_get_style_pad_left( lv_menu_get_main_header( menu ), 0 ), 0 )

section = lv_menu_section_create( root_page )

cont = create_text( section, LV_SYMBOL_SETTINGS, "Mechanics", LV_MENU_ITEM_BUILDER_VARIANT_1 )

lv_menu_set_load_page_event( menu, cont, sub_mechanics_page )

cont = create_text( section, LV_SYMBOL_AUDIO, "Sound", LV_MENU_ITEM_BUILDER_VARIANT_1 )

lv_menu_set_load_page_event( menu, cont, sub_sound_page )

cont = create_text( section, LV_SYMBOL_SETTINGS, "Display", LV_MENU_ITEM_BUILDER_VARIANT_1 )

lv_menu_set_load_page_event( menu, cont, sub_display_page )

create_text( root_page, nil, "Others", LV_MENU_ITEM_BUILDER_VARIANT_1 )

section = lv_menu_section_create( root_page )

cont = create_text( section, nil, "About", LV_MENU_ITEM_BUILDER_VARIANT_1 )

lv_menu_set_load_page_event( menu, cont, sub_about_page )

cont = create_text( section, LV_SYMBOL_SETTINGS, "Menu mode", LV_MENU_ITEM_BUILDER_VARIANT_1 )

lv_menu_set_load_page_event( menu, cont, sub_menu_mode_page )

lv_menu_set_sidebar_page( menu, root_page )

lv_event_send( lv_obj_get_child( lv_obj_get_child( lv_menu_get_cur_sidebar_page( menu ), 0 ), 0 ), LV_EVENT_CLICKED, nil )

lvgl.loop()