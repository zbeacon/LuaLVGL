local lvgl = require( "lvgl" )

setmetatable( _G, { __index = function( self, index )

	return rawget( self, index ) or rawget( lvgl.api, index )
end } )

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

local scr = lv_scr_act()

local menu = lv_menu_create( scr )

lv_obj_set_size( menu, lv_disp_get_hor_res(), lv_disp_get_ver_res() )

lv_obj_center( menu )

local sub_page = lv_menu_page_create( menu )

local cont = lv_menu_cont_create( sub_page )

local label = lv_label_create( cont )

lv_label_set_text( label, "Hello, I am hiding here" )

local main_page = lv_menu_page_create( menu )

cont = lv_menu_cont_create( main_page )

label = lv_label_create( cont )

lv_label_set_text( label, "Item 1" )

cont = lv_menu_cont_create( main_page )

label = lv_label_create( cont )

lv_label_set_text( label, "Item 2" )

cont = lv_menu_cont_create( main_page )

label = lv_label_create( cont )

lv_label_set_text( label, "Item 3 (Click me!)" )

lv_menu_set_load_page_event( menu, cont, sub_page )

lv_menu_set_page( menu, main_page )

lvgl.loop()