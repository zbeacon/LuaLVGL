# 简介

- LuaLVGL-Simulator 旨在为想要探索 LVGL 图形库应用开发的学习者提供一个便捷的 Windows 模拟环境。
- 非常适合不擅长在 Windows 开发 GUI 应用的嵌入式开发人员利用该工具开发调试或实用小工具。

# 技术架构

- **脚本语言：** Lua 5.4，以其轻量级和易学性，简化 GUI 编程过程。
- **核心组件：** LVGL 8.4，提供丰富的图形元素和高效的渲染能力。

# 为什么选择LuaLVGL-Simulator?

- **无硬件学习体验：** 无需实体硬件设备，即可在个人电脑上模拟 LVGL 应用开发，让学习之路畅通无阻。
- **Lua语言的优势：** 针对 C 语言学习曲线陡峭的问题，本项目采用 Lua 作为编程语言，以其简洁的语法和快速的学习周期，降低入门门槛，使初学者能够快速上手 GUI 开发。
- **无缝API对接：** 适配的 Lua 接口与 LVGL 原生 C API 保持高度一致，意味着从 C 语言环境（如 NXP 的 GUI-Guider 软件）迁移过来的代码只需做最少的调整即可在 Lua 环境中运行，极大地促进了代码复用和学习资源的共享。
- **跨平台开发的未来：** 项目规划中包含在嵌入式系统（如 Linux 或 RTOS ）上部署 Lua+LVGL 的方案，使得在 Windows 环境下开发的应用能够无缝移植至目标平台，极大地提升了开发效率和灵活性，实现一次编写，处处运行的理想。

# 帮助文档

[Lua 5.4](https://www.lua.org/manual/5.4/)

[LVGL](https://docs.lvgl.io/master/index.html)

# 快速开始

- 克隆项目至本地

```Shell
git clone https://gitcode.com/zbeacon/LuaLVGL.git
```

- 执行示例代码，比如 btn

```Shell
# 这里假设你在 D 盘 gitcode 文件夹下克隆项目

D:\gitcode\LuaLVGL\bin\lua54.exe D:\gitcode\LuaLVGL\examples\btn\lv_example_btn_1.lua
```

# 程序示例模板

**模板一：**

```Lua
local lvgl = require( "lvgl" )

lvapi = lvgl.api

lvgl.init( {

	hor_res = 480,  -- 屏幕宽度
	ver_res = 480,  -- 屏幕高度
} )

-- TODO:

lvgl.loop()
```

> 缺点：LVGL 的所有 API 及定义在 `lvgl.api` 中，在书写代码时都要加上前缀 `lvgl.api`

**模板二：**

```Lua
local lvgl = require( "lvgl" )

-- 将 lvgl.api 引入至全局空间，避免书写 lvgl.api 前缀，移植 C 代码时非常方便

setmetatable( _G, { __index = function( self, index )

	return rawget( self, index ) or rawget( lvgl.api, index )
end } )

lvgl.init( {

	hor_res = 480,
	ver_res = 480,
} )

-- TODO:

lvgl.loop()
```
